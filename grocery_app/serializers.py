from rest_framework import serializers
from grocery_app.models import Product, Location, Payment, Customer, Order, LineItem, OrderStatus, Courier

__author__ = 'william'


class ProductSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = ['id', 'name', 'category', 'units', 'metric', 'price', 'number_in_stock', 'photo']


class LocationSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Location
        fields = ['id', 'latitude', 'longitude', 'street_address']


class CustomerSerializer(serializers.HyperlinkedModelSerializer):
    location = LocationSerializer(many=False, read_only=True)

    class Meta:
        model = Customer
        fields = ['id', 'first_name', 'last_name', 'email_address', 'customer_number', 'location']


class LineItemSerializer(serializers.HyperlinkedModelSerializer):
    product = ProductSerializer(many=False, read_only=True)

    class Meta:
        model = LineItem
        fields = ['id', 'quantity', 'product', 'sub_total']


class OrderSerializer(serializers.HyperlinkedModelSerializer):
    line_items = LineItemSerializer(many=True, read_only=True)

    class Meta:
        model = Order
        fields = ['id', 'time_stamp', 'line_items', 'is_assigned']


class PaymentSerializer(serializers.HyperlinkedModelSerializer):
    order = OrderSerializer(many=False, read_only=True)

    class Meta:
        model = Payment
        fields = ['id', 'order', 'payment_mode', 'amount_paid']


class OrderStatusSerializer(serializers.HyperlinkedModelSerializer):
    # order_id = serializers.PrimaryKeyRelatedField(queryset=Order.objects.all(), source='order.id')
    order = OrderSerializer(many=False, read_only=True)

    class Meta:
        model = OrderStatus
        fields = ['id', 'note', 'delivery_time', 'order']


class CourierSerializer(serializers.HyperlinkedModelSerializer):
    deliveries = OrderStatusSerializer(many=True, read_only=True)
    print deliveries

    class Meta:
        model = Courier
        fields = ['first_name', 'last_name', 'on_job_status', 'order_stack', 'deliveries']