import datetime
from django.db import models

# Create your models here.

PRODUCT_CATEGORIES = (('VEGETABLES', 'VEGETABLES'),
                      ('FRUITS', 'FRUITS'),
                      ('GRAIN', 'GRAIN'),
                      ('FLOUR', 'FLOUR'),
                      ('CEREAL', 'CEREAL'),
                      ('MEAT', 'MEAT'),
                      ('POULTRY', 'POULTRY'),
                      ('FISH', 'FISH'),
                      ('SPICES', 'SPICES'),
                      ('COOKING_OIL', 'COOKING_OIL'),)

UNITS = (('KG', 'KG'),
         ('GRAMS', 'GRAMS'),
         ('MG', 'MG'),
         ('LITERS', 'LITERS'),
)

PAYMENT_MODES = (('MOBILE_MONEY', 'MOBILE_MONEY'),
                 ('CASH', 'CASH'),
                 ('CREDIT_CARD', 'CREDIT_CARD'),)

COURIER_STATUS = (('ON_JOB', 'ON_JOB'),
                  ('FREE', 'FREE'),)

ORDER_STACK = (('LOADED', 'LOADED'),
               ('PARTLY', 'PARTLY'),
               ('EMPTY', 'EMPTY'),)

ORDER_STATUS = (('PICKED', 'PICKED'),
                ('DELIVERED', 'DELIVERED'),
                ('FAILED', 'FAILED'),
                ('CANCELLED', 'CANCELLED'),)


class Product(models.Model):
    name = models.CharField(max_length=50)
    category = models.CharField(max_length=50, choices=PRODUCT_CATEGORIES)
    metric = models.FloatField()
    units = models.CharField(max_length=10, choices=UNITS)
    price = models.IntegerField()
    photo = models.ImageField(upload_to='photos/')
    number_in_stock = models.IntegerField()

    def __str__(self):
        return '{0} {1} {2} UGX {3}'.format(self.name, self.metric, self.units, self.price)


class Location(models.Model):
    latitude = models.FloatField()
    longitude = models.FloatField()
    street_address = models.CharField(max_length=100)

    def __str__(self):
        return 'LAT {0} LNG {1} ADDR {2}'.format(self.latitude, self.longitude, self.street_address)


class Customer(models.Model):
    first_name = models.CharField(max_length=10)
    last_name = models.CharField(max_length=10)
    email_address = models.EmailField(max_length=100)
    contact_number = models.CharField(max_length=20)
    location = models.ForeignKey(Location)

    def __str__(self):
        return '{0} {1} {2}'.format(self.first_name, self.last_name, self.contact_number)


class Order(models.Model):
    customer = models.ForeignKey(Customer)
    time_stamp = models.DateTimeField(default=datetime.datetime.now)
    is_assigned = models.BooleanField(default=False)

    def __str__(self):
        return '{0} {1} {2}'.format(self.customer, self.time_stamp, self.id)


class Payment(models.Model):
    order = models.ForeignKey(Order)
    payment_mode = models.CharField(max_length=20, choices=PAYMENT_MODES)
    amount_paid = models.IntegerField()

    def __str__(self):
        return '{0} {1} {2}'.format(self.order, self.payment_mode, self.amount_paid)


class LineItem(models.Model):
    quantity = models.IntegerField()
    product = models.ForeignKey(Product)
    order = models.ForeignKey(Order, related_name='line_items')
    sub_total = models.IntegerField()

    def __str__(self):
        return '{0} {1} {2}'.format(self.product, self.order, self.sub_total)


class Courier(models.Model):
    first_name = models.CharField(max_length=10)
    last_name = models.CharField(max_length=10)
    on_job_status = models.CharField(max_length=10, choices=COURIER_STATUS)
    order_stack = models.CharField(max_length=10, choices=ORDER_STACK)
    user_name = models.CharField(max_length=10)
    password = models.CharField(max_length=10)

    def __str__(self):
        return '{0} {1} {2} {3}'.format(self.first_name, self.last_name, self.user_name, self.password)


class OrderClerk(models.Model):
    first_name = models.CharField(max_length=10)
    last_name = models.CharField(max_length=10)
    user_name = models.CharField(max_length=10)
    password = models.CharField(max_length=10)

    def __str__(self):
        return '{0} {1} {2} {3}'.format(self.first_name, self.last_name, self.user_name, self.password)


class OrderStatus(models.Model):
    courier = models.ForeignKey(Courier, related_name='deliveries')
    order = models.ForeignKey(Order, related_name='orders')
    assigning_clerk = models.ForeignKey(OrderClerk)
    note = models.TextField()
    delivery_time = models.DateTimeField(null=True)
    order_status = models.CharField(max_length=20, choices=ORDER_STATUS)

    def __str__(self):
        return '{0} {1}'.format(self.courier, self.order_status)
