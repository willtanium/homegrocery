# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Courier',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=10)),
                ('last_name', models.CharField(max_length=10)),
                ('on_job_status', models.CharField(max_length=10, choices=[(b'ON_JOB', b'ON_JOB'), (b'FREE', b'FREE')])),
                ('order_stack', models.CharField(max_length=10, choices=[(b'LOADED', b'LOADED'), (b'PARTLY', b'PARTLY'), (b'EMPTY', b'EMPTY')])),
                ('user_name', models.CharField(max_length=10)),
                ('password', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='Customer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=10)),
                ('last_name', models.CharField(max_length=10)),
                ('email_address', models.EmailField(max_length=100)),
                ('contact_number', models.CharField(max_length=20)),
            ],
        ),
        migrations.CreateModel(
            name='LineItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('quantity', models.IntegerField()),
                ('sub_total', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('latitude', models.FloatField()),
                ('longitude', models.FloatField()),
                ('street_address', models.CharField(max_length=100)),
            ],
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_stamp', models.DateTimeField(default=datetime.datetime.now)),
                ('is_assigned', models.BooleanField(default=False)),
                ('customer', models.ForeignKey(to='grocery_app.Customer')),
            ],
        ),
        migrations.CreateModel(
            name='OrderClerk',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=10)),
                ('last_name', models.CharField(max_length=10)),
                ('user_name', models.CharField(max_length=10)),
                ('password', models.CharField(max_length=10)),
            ],
        ),
        migrations.CreateModel(
            name='OrderStatus',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('note', models.TextField()),
                ('delivery_time', models.DateTimeField(null=True)),
                ('order_status', models.CharField(max_length=20, choices=[(b'PICKED', b'PICKED'), (b'DELIVERED', b'DELIVERED'), (b'FAILED', b'FAILED'), (b'CANCELLED', b'CANCELLED')])),
                ('assigning_clerk', models.ForeignKey(to='grocery_app.OrderClerk')),
                ('courier', models.ForeignKey(related_name='deliveries', to='grocery_app.Courier')),
                ('order', models.ForeignKey(to='grocery_app.Order')),
            ],
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('payment_mode', models.CharField(max_length=20, choices=[(b'MOBILE_MONEY', b'MOBILE_MONEY'), (b'CASH', b'CASH'), (b'CREDIT_CARD', b'CREDIT_CARD')])),
                ('amount_paid', models.IntegerField()),
                ('order', models.ForeignKey(to='grocery_app.Order')),
            ],
        ),
        migrations.CreateModel(
            name='Product',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('category', models.CharField(max_length=50, choices=[(b'VEGETABLES', b'VEGETABLES'), (b'FRUITS', b'FRUITS'), (b'GRAIN', b'GRAIN'), (b'FLOUR', b'FLOUR'), (b'CEREAL', b'CEREAL'), (b'MEAT', b'MEAT'), (b'POULTRY', b'POULTRY'), (b'FISH', b'FISH'), (b'SPICES', b'SPICES'), (b'COOKING_OIL', b'COOKING_OIL')])),
                ('metric', models.FloatField()),
                ('units', models.CharField(max_length=10, choices=[(b'KG', b'KG'), (b'GRAMS', b'GRAMS'), (b'MG', b'MG'), (b'LITERS', b'LITERS')])),
                ('price', models.IntegerField()),
                ('photo', models.ImageField(upload_to=b'photos/')),
                ('number_in_stock', models.IntegerField()),
            ],
        ),
        migrations.AddField(
            model_name='lineitem',
            name='order',
            field=models.ForeignKey(related_name='line_items', to='grocery_app.Order'),
        ),
        migrations.AddField(
            model_name='lineitem',
            name='product',
            field=models.ForeignKey(to='grocery_app.Product'),
        ),
        migrations.AddField(
            model_name='customer',
            name='location',
            field=models.ForeignKey(to='grocery_app.Location'),
        ),
    ]
