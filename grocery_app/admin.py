from django.contrib import admin

# Register your models here.
from grocery_app.models import Product, Location, Customer, Order, Payment, LineItem, Courier, OrderClerk, OrderStatus

admin.site.register(Product)
admin.site.register(Location)
admin.site.register(Customer)
admin.site.register(Order)
admin.site.register(Payment)
admin.site.register(LineItem)
admin.site.register(Courier)
admin.site.register(OrderClerk)
admin.site.register(OrderStatus)