from django.conf.urls import url
from grocery_app.views import CourierLogin, GetOrders, MakeOrder, UpdateDeliveryState, ReceivePayment, GetAllProducts, \
    RegisterCustomer

__author__ = 'william'


urlpatterns = [url(r'^courier_login/$', CourierLogin.as_view(), name='courier_login'),
               url(r'^get_orders/$', GetOrders.as_view(), name='get_orders'),
               url(r'^make_order/$', MakeOrder.as_view(), name='make_orders'),
               url(r'^update_delivery_status/$', UpdateDeliveryState.as_view(), name='update_delivery_status'),
               url(r'^receive_payment/$', ReceivePayment.as_view(), name='receive_payment'),
               url(r'^get_products/$', GetAllProducts.as_view(), name='get_products'),
               url(r'register_customer/$', RegisterCustomer.as_view(), name='register_customer'),
               ]