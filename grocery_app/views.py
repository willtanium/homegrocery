import datetime
from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from grocery_app.models import Courier, Customer, Order, Product, LineItem, OrderStatus, Payment, Location
from grocery_app.serializers import CourierSerializer, OrderSerializer, OrderStatusSerializer, PaymentSerializer, \
    ProductSerializer, CustomerSerializer


class GetOrders(APIView):
    """
        This is meant for the courier to get his orders on the fly
    """

    def post(self, request, format=None):
        data = request.data
        courier = Courier.objects.get(id=data['courier_id'])
        courier_serializer = CourierSerializer(courier, many=False, context={'request': request})
        if courier_serializer is not None:
            return Response(courier_serializer.data, status=status.HTTP_200_OK)
        return Response(courier_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class MakeOrder(APIView):
    """
        This is meant for the customer to place their orders
    """

    def post(self, request, format=None):
        data = request.data
        # create the order
        customer = Customer.objects.get(id=data['customer_id'])
        order = Order.objects.create(customer=customer)
        order.save()
        for item in data['lineitems']:
            product = Product.objects.get(id=item['product_id'])
            line_item = LineItem.objects.create(quantity=item['quantity'], sub_total=item['subtotal'],
                                                order=order, product=product)
            line_item.save()
        order_serializer = OrderSerializer(order, many=False, context={'request': request})
        return Response(order_serializer.data, status=status.HTTP_201_CREATED)


class CourierLogin(APIView):
    """
        This simply logs the courier into the system
    """

    def post(self, request, format=None):
        data = request.data
        courier = Courier.objects.get(user_name=data['username'], password=data['password'])
        courier_serializer = CourierSerializer(courier, many=False, context={'request': request})
        if courier_serializer is not None:
            return Response(courier_serializer.data, status=status.HTTP_200_OK)
        return Response(courier_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UpdateDeliveryState(APIView):
    """
        This allows for the courier to update the state of the delivery, this is only for the case of failure
        or cancellation of the order
    """

    def post(self, request, format=None):
        data = request.data

        if 'courier_id' in data.keys:
            courier = Courier.objects.get(id=data['courier_id'])
            order = Order.objects.get(id=data['order_id'])
            order_status = OrderStatus.objects.get(courier=courier, order=order)
        if 'order_id' in data.keys:
            order = Order.objects.get(id=data['order_id'])
            order_status = OrderStatus.objects.get(order=order)

        order_status.order_status = data['status']
        order_status.save()
        order_status_serializer = OrderStatusSerializer(order_status, context={'request': request})
        return Response(order_status_serializer.data, status=status.HTTP_200_OK)


class ReceivePayment(APIView):
    """
        This authorizes a payment from a customer on delivery
    """

    def post(self, request, format=None):
        data = request.data
        order = Order.objects.get(id=data['order_id'])
        payment = Payment.objects.create(order=order, payment_mode=data['payment_mode'],
                                         amount_paid=data['amount_paid'])
        order_status = OrderStatus.objects.get(order=order, courier=Courier.objects.get(id=data['courier_id']))
        order_status.order_status = data['status']
        order_status.delivery_time = datetime.datetime.now()
        order_status.save()
        payment.save()
        payment_serializer = PaymentSerializer(payment, many=False, context={'request': request})
        return Response(payment_serializer.data, status=status.HTTP_201_CREATED)


class GetAllProducts(APIView):
    def get(self, request, format=None):
        products = Product.objects.all()
        product_serializer = ProductSerializer(products, many=True, context={'request': request})
        return Response(product_serializer.data, status=status.HTTP_200_OK)


class RegisterCustomer(APIView):
    def post(self, request, format=None):
        data = request.data
        location_data = data['location']
        location = Location.objects.create(latitude=location_data['latitude'], longitude=location_data['longitude'],
                                           street_address=location_data['street_address'])
        location.save()
        customer = Customer.objects.create(first_name=data['first_name'], last_name=data['last_name'],
                                           email_address=data['email_address'], contact_number=data['contact_number'],
                                           location=location)
        customer.save()

        customer_serializer = CustomerSerializer(customer, many=False, context={'request': request})
        return Response(customer_serializer.data, status=status.HTTP_201_CREATED)